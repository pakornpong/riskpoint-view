import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import router from './router'
import './assets/css/font.css'
import './assets/css/styles.css'
import store from './store'
import * as VueGoogleMaps from 'vue2-google-maps'
import excel from 'vue-excel-export'
import GmapCluster from "vue2-google-maps/dist/components/cluster"

Vue.component("cluster", GmapCluster);
Vue.use(excel);
Vue.use(require('vue-moment'));
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyBlEky4t6pYY_4MIbsvssO5aE1ecLK3kH0',
    libraries: 'places',
  },
  installComponents: true
})
Vue.config.productionTip = false

new Vue({
  vuetify,
  router,
  store,

  render: h => h(App),
}).$mount('#app')
