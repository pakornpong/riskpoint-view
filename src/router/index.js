import Vue from 'vue'
import VueRouter from 'vue-router'
import Riskpoint from "../views/Riskpoint";
import Patient from "../views/Patient";

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Riskpoint',
    component: Riskpoint
  },
  {
    path: '/patient/:riskpoint/:oncenterlet/:oncenterlng',
    name: 'Patient',
    component: Patient
  }
];

const router = new VueRouter({
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router
