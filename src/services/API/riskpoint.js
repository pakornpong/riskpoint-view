import Service from '../Service'

const riskpoint = {

    getPageRiskpoint(page) {
        let p = page ? page : 1;
        return Service.get(`/api/riskpoint?page=${p}`);
    },
    getAllRiskpoint(){
        return Service.get(`/api/riskpoint`);
    },
    getRiskpointdatetime(start_date,end_date,start_time,end_time,start_die,end_die,district) {
        return Service.get(`/api/riskpoint?start_date=${start_date}&end_date=${end_date}&start_time=${start_time}&end_time=${end_time}&start_die=${start_die}&end_die=${end_die}&district=${district}`)
    }

};
export  default riskpoint;
