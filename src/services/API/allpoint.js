import Service from '../Service'

const allpoint = {
    getAllPoint(){
        return Service.get(`/api/allpoint`);
    },
    getAllpointdatetime(start_date,end_date,start_time,end_time,start_die,end_die,district) {
        return Service.get(`/api/allpoint?start_date=${start_date}&end_date=${end_date}&start_time=${start_time}&end_time=${end_time}&start_die=${start_die}&end_die=${end_die}&district=${district}`)
    },

};
export  default allpoint;
