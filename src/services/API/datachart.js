import Service from "../Service";

const datachart = {

    getDatachart(id){
        return Service.get(`/api/graphdata?id=${id}`);
    }
};
export  default datachart;
