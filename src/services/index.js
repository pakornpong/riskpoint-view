import riskpoint from "./API/riskpoint"
import patient from "./API/patient"
import datachart from "./API/datachart"
import allpoint from "./API/allpoint"

export default {
    riskpoint,
    patient,
    datachart,
    allpoint
}
