import Vue from 'vue';
import Vuex from 'vuex';
import Riskpoint from './modules/Riskpoint';
import Patient from './modules/Patient';
import Datachart from './modules/Datachart';
import Allpoint from './modules/Allpoint'

import createPersistedState from 'vuex-persistedstate'
Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        riskpoint: Riskpoint,
        patient: Patient,
        datachart: Datachart,
        allpoint: Allpoint
    },
    plugins: [createPersistedState()]
})
