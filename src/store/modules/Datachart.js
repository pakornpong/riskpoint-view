import Service from '../../services'

const state = {
    alldatachart: [],
};
const getters = {

};
const actions = {
    onGetDatachart({commit},id) {
        return Service.datachart.getDatachart(id).then(res => {
            commit('setAlldatachart',res);
        })
    }
};
const mutations = {
    setAlldatachart(state,data) {
        state.alldatachart = data;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}
