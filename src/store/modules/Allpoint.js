import Service from '../../services'

const state = {
    allPoint: [],
};
const getters = {

};
const actions = {
    onGetAllpointdatetime({commit},date_range){
        return Service.allpoint. getAllpointdatetime(date_range.start_date,date_range.end_date,date_range.start_time,date_range.end_time,date_range.start_die,date_range.end_die,date_range.district).then(res => {
            commit('setAllPoint',res);
        });
    },
    onGetAllpoint({commit}) {
        return Service.allpoint.getAllPoint().then(res => {
            commit('setAllPoint',res);
        });
    },
};
const mutations = {
    setAllPoint(state,data) {
        state.allPoint = data;
    },
};

export default  {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
}
