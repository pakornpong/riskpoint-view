import Service from '../../services'

const state = {
    allRiskpoint: [],
    pageriskpoint: [],
    selectedDistrict: "",

};
const getters = {

};
const actions = {
    onGetPageRiskpoint({commit},page) {
        return Service.riskpoint.getPageRiskpoint(page).then(res => {
            commit('setPageRiskpoint',res);
            return res
        });
    },
    onGetAllRiskpoint({commit}) {
        return Service.riskpoint.getAllRiskpoint().then(res => {
            commit('setAllRiskpoint',res);
        });
    },
    onGetRiskpointdatetime({commit},date_range){
        return Service.riskpoint.getRiskpointdatetime(date_range.start_date,date_range.end_date,date_range.start_time,date_range.end_time,date_range.start_die,date_range.end_die,date_range.district).then(res => {
            commit('setAllRiskpoint',res);
        });
    },
    onSetSelectedDistrict({commit},district) {
        commit('setSelectedDistrict', district);
    },

};
const mutations = {
    setAllRiskpoint(state,data) {
        state.allRiskpoint = data;
    },
    setPageRiskpoint(state,data) {
        state.pageriskpoint = data;
    },
    setSelectedDistrict(state,data) {
        state.selectedDistrict = data;
    },

};

export default  {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
}
