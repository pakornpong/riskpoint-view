import Service from '../../services'

const state = {
    allPatient: [],
    Patient:{}
};
const getters = {

};
const actions = {
    onGetAllPatient({commit},page) {
        return Service.patient.getAllPatient(page).then(res => {
            commit('setPatient',res);
        });
    },
    onGetPatient({commit},id) {
        return Service.patient.getPatient(id).then(res => {
            commit('setAllPatient',res);
        });
    },
    onGetPatientdatetime({commit},date_range){
        return Service.patient.getPatientdatetime(date_range.start_date,date_range.end_date,date_range.start_time,date_range.end_time,date_range.start_die,date_range.end_die,date_range.district).then(res => {
            commit('setAllPatient',res);
        });
    }
};
const mutations = {
    setAllPatient(state,data) {
        state.allPatient = data;

    },
    setPatient(state,data) {
        state.Patient = data;

    }
};

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}
